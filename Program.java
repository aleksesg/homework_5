import java.util.Scanner;
class Program{
	public static void main(String[] args) {
		System.out.println("min digit from value = "+minDigit());
	}
	static int minDigit(){
		Scanner scanner = new Scanner(System.in);
		int minDigit=9;
		int i = scanner.nextInt();
		while(i!=-1){
			int var;
			while(i!=0){
				var = i%10;
				if (var<minDigit)
					minDigit=var;
				i/=10;
			}
			i=scanner.nextInt();
		}
		return minDigit;
	}
}
